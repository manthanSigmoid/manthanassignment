Assignment : 

1- Create a java program to read data from csv and then set it to proto objects and print them using these proto objects.

Proto files to be included :

Employee.proto
Example fields can be : name, employee_id, building_code, floor_number (this should be enum), salary, department etc
Building.proto
Example fields can be : building_code, total_floors, companies_in_the_building, cafteria_code

Create data for 100 people and 10 building with similar signature.

Assignment : 

ASSIGNMENT TO INCLUDE HBASE, PROTOBUF and MR

2 - Create a Mapreduce job to use the above people and building data, load proto objects with those values and then write it back to HBase.
While doing this drill, update the employee.proto file to include cafeteria_code as a new field. Leave the value blank for now.
Build jars for the above proto project, this will be used for downstream assignments

3 - Create a MR job using the above created table (employee and building).
Read both the tables using there jars added to the lib
Parse its value using proto objects
Join both the tables on the basis of building_code and enrich the cafteria_code for each employee using the cafteria_code available in the building proto object and write it back to hbase table.
Construct a decent unique rowkey for denoting each row of value.


4 - Create a proto attendance.proto with fields like employee_id, date and generate random entries for different dates for each employee.
For every building find the employee with the lowest attendance. 
