//Task : III
//tried to create as generic functions as I can to avoid big codes and vulnerabilities associated with them
//like create table & boilerplate
//how can I write one single function for set? (setting data to different HBASE table)
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import java.util.NavigableMap;
import java.io.IOException;
class HbaseOperations{

    public void createTable(String tableName, String familyName) throws Exception {
        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Admin admin = connection.getAdmin();
        if (!admin.tableExists(TableName.valueOf(tableName))) {
            System.out.println("Creating a table");
            HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));
            tableDescriptor.addFamily(new HColumnDescriptor(familyName));
            admin.createTable(tableDescriptor);
            System.out.println("Created" + tableName + "table!");
        }
    }

    private static Table boilerplate(String tableName) throws Exception{
        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Table table = connection.getTable(TableName.valueOf(tableName));
        return table;
    }

    public void setEmployeeToHbase(String tableName, String familyName, Employee.Person.Builder newEmployee)  throws Exception {
        Put putObj = new Put(Bytes.toBytes(newEmployee.getEmpId()));
        putObj.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("data"), newEmployee.build().toByteArray());//family, qualifier, value
        boilerplate(tableName).put(putObj);
    }

    public void setBuildingToHbase(String tableName, String familyName, BuildingProto.Building.Builder newBuilding) throws Exception {
        Put putObj = new Put(Bytes.toBytes(newBuilding.getBuildingCode()));
        putObj.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("data"), newBuilding.build().toByteArray());//family, qualifier, value
        boilerplate(tableName).put(putObj);
    }

    private static void printAllValues(Result result) {
        //col family, col name, timeStamp & val
        NavigableMap<byte[], NavigableMap<byte[], NavigableMap<Long, byte[]>>> resultMap = result.getMap();
        for (byte[] colFamily : resultMap.keySet()) {
            String cf = Bytes.toString(colFamily);
            NavigableMap<byte[], NavigableMap<Long, byte[]>> colMap = resultMap.get(colFamily);

            for (byte[] colName : colMap.keySet()) {
                String col = Bytes.toString(colName);
                NavigableMap<Long, byte[]> timeStampMap = colMap.get(colName);

                for (Long timeStamp : timeStampMap.keySet()) {
                    String ts = timeStamp.toString();
                    String value = Bytes.toString(timeStampMap.get(timeStamp));
                    System.out.println("CF: " + cf + "col: " + col + "value: " + value);
                }
            }
        }
    }

    public void getDataFromHabse(String tableName) throws IOException {
        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Table table = connection.getTable(TableName.valueOf(tableName));
        Scan fullScan = new Scan();
        ResultScanner fullScanResult = table.getScanner(fullScan);
        for (Result r : fullScanResult) {
            printAllValues(r);
        }
        fullScanResult.close();
    }
}