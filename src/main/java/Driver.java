//Task: V
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;


import java.io.BufferedReader;
import java.io.IOException;


public class Driver {
    static String EMPLOYEETABLE = "employee";
    static String EMPLOYEEFAMILY = "employee";

    static String BUILDINGTABLE = "building";
    static String BUILDINGFAMILY = "building";

    public static void main(String ... args) throws Exception {
        final String employeeFile = "/Users/manthan/Documents/csv/Employee.csv";
        final String buildingFile = "/Users/manthan/Documents/csv/Building.csv";

//reading from Employee.CSV - Putting into Person, Putting into employee table in HBASE
        ReadFromCsv.writeDataToHBASE(employeeFile, EMPLOYEETABLE, EMPLOYEEFAMILY);
        ReadFromCsv.writeDataToHBASE(buildingFile, BUILDINGTABLE, BUILDINGFAMILY);

//        Driver
//        boilerPlate
        Configuration conf = HBaseConfiguration.create();
        Job job = new Job(conf, "Assignment");
        job.setJarByClass(Driver.class); //main() of Driver class

//      scan from tables
        Scan scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);

//        set Map class and properties
        TableMapReduceUtil.initTableMapperJob(EMPLOYEETABLE, scan, Mapper.class, Text.class, ImmutableBytesWritable.class, job);
//        sourceTable, scan obj to read, Mapper class, op data types of Mapper
        TableMapReduceUtil.initTableMapperJob(BUILDINGTABLE, scan, Mapper.class, Text.class, ImmutableBytesWritable.class, job);

//        set Reduce class and properties
        TableMapReduceUtil.initTableReducerJob(EMPLOYEETABLE, Reducer.class,job);
        job.setNumReduceTasks(1); //nms of reduce tasks, increase for parallel computation

        boolean b = job.waitForCompletion(true);
        if(!b)
            throw  new Exception("error with the job!!");
    }
}
