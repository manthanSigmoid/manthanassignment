//Task: IV
//Key => building code, values => objects (employee or building)
//for future references separate the objects with different taggers

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import java.io.IOException;


public class Mapper extends TableMapper<Text, ImmutableBytesWritable> { //output <key, values>
    public static final String CF = "attributes";
    public static final String CQ = "data";

    //output
    private Text key = new Text();
    private ImmutableBytesWritable opValue = new ImmutableBytesWritable();

    //tableNames
    private static String employeeTableName = "employee";
    private static String buildingTableName = "building";

    public void map(ImmutableBytesWritable row, Result value, Context context) throws IOException, InterruptedException{
        // row => row id of the result table
        // values => cols and values for a particular rowid
        //context => process row and val and put then into context
        System.out.println("------MAPPER------");
        TableSplit currentSplit = (TableSplit) context.getInputSplit();
        String tableName = currentSplit.getTable().getNameAsString();

        //tagger
        TaggerProto.Tagger.Builder taggerProto = TaggerProto.Tagger.newBuilder();

        try {
            if (employeeTableName.equals(tableName)) {
                byte[] employeeRow = value.getValue(Bytes.toBytes(CF), Bytes.toBytes(CQ));
                Employee.Person person = Employee.Person.parseFrom(employeeRow);
                //key, value => buildingCode, row
                key = new Text(person.getBuildingCode());
                taggerProto.setActualObject(Employee.Person.parseFrom(employeeRow).toByteString());
                taggerProto.setTag(TaggerProto.Tagger.component_tag.valueOf("Employee"));
                opValue = new ImmutableBytesWritable(taggerProto.build().toByteArray());
                key.set(employeeRow);
                context.write(key, opValue);
            }else if(buildingTableName.equals(tableName)){
                byte[] buildingRow = value.getValue(Bytes.toBytes(CF), Bytes.toBytes(CQ));
                BuildingProto.Building building = BuildingProto.Building.parseFrom(buildingRow);
                //key, value => buildingCode, row
                //Text, ByteArray
                key = new Text(building.getBuildingCode());
                key.set(buildingRow);
                taggerProto.setTag(TaggerProto.Tagger.component_tag.valueOf("Building"));
                taggerProto.setActualObject(BuildingProto.Building.parseFrom(buildingRow).toByteString());
                opValue = new ImmutableBytesWritable(taggerProto.build().toByteArray());
                context.write(key, opValue);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

