class Person{
    String name;
    String emp_id;
    String building_code;
    String salary;
    String department;
    String cafe_code;

    public Person(String name, String emp_id, String building_code, String salary, String department, String cafe_code){
        this.name = name;
        this.emp_id = emp_id;
        this.building_code = building_code;
        this.salary = salary;
        this.department = department;
        this.cafe_code = cafe_code;
    }
    public void printPerson(){
        //System.out.println(this.cafe_code);
    }
}