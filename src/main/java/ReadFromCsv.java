import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


/*
 *Task: I
 *   Reading from CSV
 *   Create a Person class
 *   Set & get from Person
 * Task: II
 *   create Proto (Employee & Building)
 *   Set & Get from Proto
 * Task: III
 *   Create an HBASE (Employee & Building)
 *   Set & Get from HBASE
 * Task: IV
 * MR
 *   MAPPER
 *   REDUCER
 *   DRIVER
 * TASK: V
 *   GS Style code
 * */

public class ReadFromCsv {
    static String EMPLOYEETABLE = "employee";
    static String EMPLOYEEFAMILY = "employee";

    static String BUILDINGTABLE = "building";
    static String BUILDINGFAMILY = "building";

    static String line = "";
    static String csvSplitBy = ",";

    static void writeDataToHBASE(String fileUri, String tableName, String colFamily) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(fileUri));
        try {
            HbaseOperations HbObj = new HbaseOperations();
//            System.out.println("inside try");
            while ((line = br.readLine()) != null) {
                String[] employee = line.split(csvSplitBy);
                //System.out.println(employee[0]);

//                Task: I Putting into Person
                if (tableName.equals(EMPLOYEETABLE)) {
//                    Person newPerson = new Person(employee[0], employee[1], employee[2], employee[3], employee[4], employee[5]);
//                    newPerson.printPerson();

                    //Task II: Employee Proto
                    Employee.Person.Builder newEmployee = Employee.Person.newBuilder();
                    newEmployee.setName(employee[0]);
                    newEmployee.setEmpId(Integer.parseInt(employee[1]));
                    newEmployee.setBuildingCode(employee[2]);
                    newEmployee.setFloor(Employee.Person.Floor.valueOf(Integer.parseInt(employee[3])));
                    newEmployee.setSalary(Integer.parseInt(employee[4]));
                    newEmployee.setDepartment(employee[5]);
//                    System.out.println(newEmployee.getName());
//                    System.out.println(newEmployee.getBuildingCode());

                    //Task: III Putting into employee table in HBASE
                    HbObj.setEmployeeToHbase(EMPLOYEETABLE, EMPLOYEEFAMILY, newEmployee);//tableName, familyName, proto
                } else if (tableName.equals(BUILDINGTABLE)) {
                    br = new BufferedReader(new FileReader(fileUri));
                    line = "";
                    while ((line = br.readLine()) != null) {
//                      System.out.println("Readnig bulding.csv");
                        String[] building = line.split(csvSplitBy);
//                      System.out.println(building[0]);

                        BuildingProto.Building.Builder newBuilding = BuildingProto.Building.newBuilder();
                        newBuilding.setBuildingCode(building[0]);
                        newBuilding.setTotalFloors(Integer.parseInt(building[1]));
                        newBuilding.setCompanies(building[2]);
                        newBuilding.setCafeteriaCode(building[3]);

                        System.out.println(newBuilding.getBuildingCode());
                        HbaseOperations HbObjBuilding = new HbaseOperations();
                        HbObjBuilding.setBuildingToHbase(BUILDINGTABLE, BUILDINGFAMILY, newBuilding);
                    }
                }
//                HbObj.getDataFromHabse(EMPLOYEETABLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}