//Task: IV
//parse the values and seperate employee objs and building objs
//get cafeCode from building obj and associate with each employee in the values
//update the employee table

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import static org.apache.hadoop.hbase.client.ConnectionFactory.createConnection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;



public class Reducer extends TableReducer<Text, ImmutableBytesWritable, ImmutableBytesWritable> {
    //ip key type, ip value typr, op key type, op value (PUT) set auto
    private static String CF_employee= "employee";
    private static String CQ = "data";
    private ArrayList<ImmutableBytesWritable> employeeList = new ArrayList<ImmutableBytesWritable>();
    private ArrayList<ImmutableBytesWritable> buildingList = new ArrayList<ImmutableBytesWritable>();

    //tableNames
    private static String employeeTableName = "employee";
    private static String buildingTableName = "building";

    public void reduce(Text key, Iterable <ImmutableBytesWritable> value, Context context) throws IOException{
        System.out.println("------REDUCER------");
        Iterator<ImmutableBytesWritable> itr = value.iterator();
        //separating emmployee Objs and building objects
        while(itr.hasNext()){
            ImmutableBytesWritable data = itr.next();
            TaggerProto.Tagger taggerProto =  TaggerProto.Tagger.parseFrom(data.get());
            String tableName = taggerProto.getTag().toString();
            if(employeeTableName.equals(tableName)){
                employeeList.add(data);
            }
            else if(buildingTableName.equals(tableName)){
                buildingList.add(data);
            }
            updateEmployeeTable(employeeList,buildingList);
        }
    }
    private void updateEmployeeTable(ArrayList<ImmutableBytesWritable> employeeList, ArrayList<ImmutableBytesWritable> buildingList)
            throws IOException{
        if(buildingList.size()==1){
            byte[] building = buildingList.get(0).get();
//            getting cafeCode from building obj and associate with each employee in the values
            String cafeteriaCode = BuildingProto.Building.parseFrom(building).getCafeteriaCode();
            for(ImmutableBytesWritable employee: employeeList){
                Employee.Person employeeObj = Employee.Person.parseFrom(employee.get());
                Employee.Person.Builder newEmployeeObj = Employee.Person.newBuilder();
                newEmployeeObj.setCafeteriaCode(cafeteriaCode);
                newEmployeeObj.setFloor(employeeObj.getFloor());
                newEmployeeObj.setBuildingCode(employeeObj.getBuildingCode());
                newEmployeeObj.setDepartment(employeeObj.getDepartment());
                newEmployeeObj.setSalary(employeeObj.getSalary());
                newEmployeeObj.setEmpId(employeeObj.getEmpId());
                newEmployeeObj.setName(employeeObj.getName());
//                update the employee table
                Put putObj = new Put(Bytes.toBytes(Integer.toString(newEmployeeObj.getEmpId())));
                putObj.addColumn(Bytes.toBytes(CF_employee),Bytes.toBytes(CQ), newEmployeeObj.build().toByteArray());//CF, CQ, value
                employeeTableBoilerPlate().put(putObj);
            }
        }
    }
    private static Table employeeTableBoilerPlate() throws IOException{
        HBaseConfiguration config = new HBaseConfiguration();
        Connection connection = createConnection(config);
        Table table = connection.getTable(TableName.valueOf(employeeTableName));
        return table;
    }
}